#pragma once
#include <stdint.h>

/*
 * Various string functions.
 *
 * 'const char* (&str)':
 * 		A reference to a pointer that will not change the contents of a string.
 * -
 * */
// All four functions are mutators!
// -
void skip_space (const char* (&str));
void skip_digit (const char* (&str));
void skip_graph (const char* (&str));
int8_t parse_int (const char* (&str));
