#include <avr/io.h>
#include "Pin.h"
#include "Pins.h"
#include "Display.h"
#include "Gifs.h"
#include "Oscilloscope.h"
#include "LED.h"
#include <stdlib.h>
#include <math.h>

int main ()
{
	Oscilloscope scope(
		Display(Pin(DDRB, PORTB, 4),
				Pin(DDRB, PORTB, 5),
				Pin(DDRB, PORTB, 6),
				Pins(DDRF, PORTF, "7 6 5 4 . . . ."),
				Pins(DDRD, PORTD, ". . . . 3 2 1 0")));
	while (true)
	{
		const uint16_t steps = 360;
		for (double a = 0; a < 2 * M_PI; a += 2.0 * M_PI / steps)
		{
			scope.push(1023.0 / 2 + 1023.0 / 2 * sin(a * 10));
			error.blink(50);
		}
	}
}
