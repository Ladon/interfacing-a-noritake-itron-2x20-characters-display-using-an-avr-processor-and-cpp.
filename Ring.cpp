#include "Ring.h"

// Explicit instantiation of template.
// 		https://docs.microsoft.com/en-us/cpp/cpp/explicit-instantiation?view=vs-2019
// -
template class Ring<uint16_t>;