#pragma once
#include <stdint.h>		// uint8_t //

class Iterator
{
public:
	Iterator (const uint8_t& start_pos = -1, const uint8_t& tmp_max = 0);
	Iterator (const Iterator& it);
	~Iterator () 							= default;
	Iterator () 							= delete;
	Iterator (Iterator&&) 					= delete;
	Iterator& operator = (const Iterator&) 	= delete;
	Iterator& operator = (Iterator&&) 		= delete;
	Iterator& operator ++ ();
	Iterator& operator += (const uint8_t& offset);
	Iterator& operator = (const uint8_t& index);
	uint8_t operator + (const uint8_t& offset) const;
	// Precondition:
	// 		'max' < 'new_max'.
	// -
	void set_max (const uint8_t& new_max);
	operator uint8_t () const;
	bool operator == (const Iterator& it) const;
	bool operator != (const Iterator& it) const;
private:
	uint8_t pos;
	uint8_t max;
};

