#include "Display.h"
// <util/delay.h>
//
#define F_CPU	16000000UL
// -
#include <util/delay.h>		// _delay_ms().. //
#include <string.h>			// strlen().. //

Display::Display (const Pin& RS, const Pin& RW, const Pin& E,
				  const Pins& upper_nibble, const Pins& lower_nibble)
	: rs(RS), rw(RW), e(E), upper(upper_nibble), lower(lower_nibble)
{
	_delay_ms(500);
	rs = 0;
	rw = 0;
	e = 0;

	set_8bit();
	clear_display();
	set_display_cursor_blink(true,false,false);
	set_cursor_display_shift(true,false);
}

// void Display::print (const char text[])
// {
	// for (uint8_t i = 0; i < strlen(text); ++i)
	// {
		// print(text[i]);
	// }
// }

void Display::print (const char text[], const uint8_t& tmp_length, const uint8_t& x, const uint8_t& y)
{
	// Find string length.
	// -
	const uint8_t length = (tmp_length != 0 ? tmp_length : strlen(text));
	write_to_display(x,y);
	data_start();
	for (uint8_t i = 0; i < length; ++i)
	{
//		print(text[i]);
		write(text[i]);
	}
	data_stop();
}

void Display::store_glyph (const uint8_t (&graph)[8], const uint8_t& position)
{
	write_to_ram(position * 8);
	data_start();
	for (uint8_t i = 0; i < 8; ++i)
	{
//		print(graph[i]);
		write(graph[i]);
	}
	data_stop();
	write_to_display();
}

void Display::store_gif (const uint8_t (&graph)[8][8])
{
	write_to_ram();
	data_start();
	for (uint8_t i = 0; i < 8; ++i)
	{
		for (uint8_t j = 0; j < 8; ++j)
		{
//			print(graph[i][j]);
			write(graph[i][j]);
		}
	}
	data_stop();
	write_to_display();
}

void Display::draw_gif (const uint8_t& x, const uint8_t& y)
{
	uint8_t address;
	address = (1 < y) ? 0xC0 : 0x80;
	address += x;
	for (uint8_t i = 0; i < 8; ++i)
	{
		write(address);
		print_char(i);
		_delay_ms(325);
	}
}

void Display::write (const uint8_t& data)
{
	// RS == 0 is assumed (for most writes)!
	// -
	upper = data;
	lower = data;
	_delay_us(1);
	e = 1;
	_delay_us(1);
	e = 0;
}

void Display::write_to_display (const uint8_t& x, const uint8_t& y)
{
	/* Equivalent code:
	uint8_t address;
	if (y == 0)
	{
		address = 0x80;
	}
	else
	{
		address = 0xC0;
	}
	address += x;
	write(address);
	- */
	write(x + (y == 0 ? 0x80 : 0xC0));
}

void Display::write_to_ram (const uint8_t& offset)
{
	write(0x40 + offset);
}

void Display::print_char (const uint8_t& i)
{
	data_start();
	write(i);
	// Reset rs-pin state.
	// -
	data_stop();
}

void Display::set_display_cursor_blink (const bool& display, const bool& cursor, const bool& blink)
{
	uint8_t data;
	data = 0x08 | (display?0x4:0) | (cursor?0x2:0) | (blink?0x1:0);
	write(data);
}

void Display::clear_display ()
{
	write(0x01);
	_delay_ms(3);
}

void Display::set_cursor_display_shift (const bool& cursor, const bool& display)
{
	/*
	 * 0, 0 : ..
	 * 1, 0 : Automatically shift cursor to the right.
	 * 0, 1 : Shift display to the right.
	 * 1, 1 : ..
	 * */
	 uint8_t data;
	 data = 0x4 | (cursor?2:0) | (display?1:0);
	 write(data);
}

// bool Display::same_representation (const uint8_t& i)
// {
	// These characters are represented the same in ASCII and in the display.
	// -
	// return (0x20 <= i && i <= 0x7D);
// }

void Display::set_8bit ()
{
	write(0x30);
	set_brightness(1);
}

// Set brightness to 100/ 75/ 50/ 25%.
// -
void Display::set_brightness (const float& f)
{
	// 0 : 100
	// 1 :  75
	// 2 :  50
	// 3 :  25
	// -
	rs = 1;
	write(4 * (1 - f));
	rs = 0;
}
