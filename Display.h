#pragma once
#include <stdint.h>	// uint8_t.. //
#include "Pin.h"
#include "Pins.h"

/*
 * Description:
 * 		Communicates with "Noritake Itron cu20025ecpb-u1j".
 * -
 * */
class Display
{
public:
	Display (const Pin& RS, const Pin& RW, const Pin& E,
			 const Pins& upper_nibble, const Pins& lower_nibble);
	Display (const Display&) 				= default;
	~Display () 							= default;
	Display () 								= delete;
	Display (Display&&) 					= delete;
	Display& operator = (const Display&) 	= delete;
	Display& operator = (Display&&) 		= delete;
	void print (const char text[], const uint8_t& tmp_length = 0, const uint8_t& x = 0, const uint8_t& y = 0);
	void set_cursor (const uint8_t& x = 0, const uint8_t& y = 0)
	{
		write_to_display(x,y);
	}
	// Single image.
	// -
	void store_glyph (const uint8_t (&graph)[8], const uint8_t& position = 0);
	// 8-frame gif.
	// -
	void store_gif (const uint8_t (&graph)[8][8]);
	void draw_gif (const uint8_t& x = 20 / 2, const uint8_t& y = 2 / 2);
	void print_char (const uint8_t& i);
	void set_cursor_display_shift (const bool& cursor = true, const bool& display = false);
private:
	Pin rs, rw, e;
	// Upper/lower nibble for the display data bus.
	// -
	Pins upper;
	Pins lower;

//	void home ()
//	{
//		write(2);
//	}
	void write (const uint8_t& data);
	void write_to_display (const uint8_t& x = 0, const uint8_t& y = 0);
	void write_to_ram (const uint8_t& offset = 0);
	void data_start ()
	{
		rs = 1;
	}
	void data_stop ()
	{
		rs = 0;
	}
	void set_display_cursor_blink (const bool& display, const bool& cursor = false, const bool& blink = false);
	void clear_display ();
//	bool same_representation (const uint8_t& i);

	// Initialization only, functions.
	// -
	void set_8bit ();
	void set_brightness (const float& f);
};
