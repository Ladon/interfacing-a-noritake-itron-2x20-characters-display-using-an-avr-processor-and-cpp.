#include "Iterator.h"

Iterator::Iterator (const uint8_t& start_pos, const uint8_t& tmp_max)
	: pos(start_pos), 
	  max(tmp_max)
{
}

Iterator::Iterator (const Iterator& it) 
	: pos(it.pos), 
	  max(it.max)
{
}

Iterator& Iterator::operator ++ ()
{
	// Circle.
	// -
	if (max < pos + 1)
	{
		pos = 0;
	}
	// Increase.
	// -
	else
	{
		++pos;
	}
	return *this;
}

Iterator& Iterator::operator += (const uint8_t& offset)
{
/*		for (uint8_t i = 0; i < offset; ++i)
	{
		operator++();
	}*/
	pos += offset;
	pos %= max + 1;
	return *this;
}

Iterator& Iterator::operator = (const uint8_t& index)
{
	pos = index;
	return *this;
}

uint8_t Iterator::operator + (const uint8_t& offset) const
{
	Iterator it(pos,max);
	it += offset;
	return it;
}

// Precondition:
// 		'max' < 'new_max'.
// -
void Iterator::set_max (const uint8_t& new_max)
{
	max = new_max;
}

Iterator::operator uint8_t () const
{
	return pos;
}

bool Iterator::operator == (const Iterator& it) const
{
	return pos == it.pos;
}

bool Iterator::operator != (const Iterator& it) const
{
	return !operator==(it);
}
