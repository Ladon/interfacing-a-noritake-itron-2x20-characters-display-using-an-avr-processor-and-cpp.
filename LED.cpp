#include "LED.h"
// <util/delay.h>.
//
#define F_CPU	16000000UL
// To do:
// 		Stop using __delay_..().
// -
#define __DELAY_BACKWARD_COMPATIBLE__ 1
// -
#include <util/delay.h>			// _delay_ms().. //
#include <avr/io.h>				// Port definitions.. //

LED error(Pin(DDRC, PORTC, 7));

LED::LED (const Pin& ppin) : pin(ppin)
{
}

void LED::toggle ()
{
	pin.toggle();
}

LED& LED::operator () (uint8_t error_code, const uint16_t& period)
{
	while (true)
	{
		// Clear "screen".
		// -
		_delay_ms(2 * period);
		const uint16_t period_header = period / 16; 			// 6.25% //
		const uint16_t period_body = period - period_header;
		for (int8_t i = 7; i >= 0; --i)
		{
			blink(period_header);
			if (error_code & 0x80)
			{
				blink(period_body);
			}
			else
			{
				_delay_ms(period_body);
			}
			error_code <<= 1;
		}
	}
}

void LED::blink (const uint16_t& ms)
{
	pin = 1;
	_delay_ms(ms / 2);
	pin = 0;
	_delay_ms(ms / 2);
}
