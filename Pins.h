#pragma once
#include <stdint.h>		// uint8_t.. //
#include <avr/io.h>		// Port defs.. //
#include "Bit_Map.h"

/*
 * Description:
 * 		Adds intuitive functions to port handling.
 * Parameters:
 * 		bit_order:
 * 			A string like "7 - 1 3 - - - -" used to conceal the bit mapping
 * 			between input and the port.
 * -
 * */
class Pins
{
public:
	Pins (volatile uint8_t& DDRX, volatile uint8_t& PORTX, const char bit_order[]);
	Pins (const Pins&) 				= default;
	~Pins () 						= default;
	Pins (Pins&&) 					= delete;
	Pins& operator = (const Pins&) 	= delete;
	Pins& operator = (Pins&&) 		= delete;
	Pins& operator = (const uint8_t& data);
private:
	volatile uint8_t& ddr;
	volatile uint8_t& port;
	uint8_t mask;
	Bit_Map map;
};
