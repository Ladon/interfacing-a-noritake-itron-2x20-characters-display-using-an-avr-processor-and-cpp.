#include "Bit_Map.h"
#include "my_string.h"

Bit_Map::Bit_Map (const char tmp_bit_order[])
{
	set_bit_order(tmp_bit_order);
}

uint8_t Bit_Map::operator () (const uint8_t& tmp_data) const
{
	uint8_t data = 0;
	for (uint8_t i = 0; i < 8; ++i)
	{
		if (bit_order[i] < 8)
		{
			// Take the (bit_order[i])th bit,
			// shift it to the first place and
			// place it in the corresponding position i.
			// -
			data |= ((tmp_data & (1 << bit_order[i])) >> bit_order[i]) << i;
		}
	}
	return data;
}

// Extract numbers from a string.
// -
// Argument is passed by value, so that we modify it in "string" functions.
// -
void Bit_Map::set_bit_order (const char bit_order_copy[])
{
	for (int8_t i = 7; i >= 0; --i)
	{
		bit_order[i] = parse_int(bit_order_copy);
		skip_graph(bit_order_copy);
	}
}
