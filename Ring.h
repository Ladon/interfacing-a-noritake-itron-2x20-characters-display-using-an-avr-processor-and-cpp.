#pragma once
#include <stdlib.h>		// malloc() //
#include "Iterator.h"

/*
 * Description:
 * 		Circular (Ring) buffer.
 * -
 * */
template <typename T>
class Ring
{
public:
	Ring (const uint8_t& length = 8, const bool& tmp_fixed_size = true);
	~Ring ();
	Ring () 						= delete;
	Ring (const Ring&) 				= delete;
	Ring (Ring&&) 					= delete;
	Ring& operator = (const Ring&) 	= delete;
	Ring& operator = (Ring&&) 		= delete;
	void push (const T& item);
	T pop ();
	T operator [] (const uint8_t& index) const;
private:
	T* data;
	uint8_t size;
	uint8_t capacity;
	Iterator first;
	Iterator last;
	const bool fixed_size;
	void extend ();
};

template <typename T>
Ring<T>::Ring (const uint8_t& length, const bool& tmp_fixed_size)
	: data(0), 
	  size(0), 
	  capacity(0), 
	  first(0,0), 
	  last(-1,0), 
	  fixed_size(tmp_fixed_size)
{
	data = static_cast<T*>(malloc(length * sizeof(T)));
	// Could not allocate memory.
	// -
	if (data == 0)
	{
		// Blink error.
		// -
	}
	// Initialize memory.
	// -
	else
	{
		for (uint8_t i = 0; i < length; ++i)
		{
			data[i] = 0;
		}
	}
	capacity = length;
	size = 0;
	first.set_max(size);
	last.set_max(size);
}

template <typename T>
Ring<T>::~Ring ()
{
	free(data);
}

template <typename T>
void Ring<T>::push (const T& item)
{
	// Limit reached!
	// -
	if (capacity < size + 1)
	{
		// Circle.
		// -
		if (fixed_size)
		{
			pop();
		}
		// Extend (act as a queue).
		// -
		else
		{
			extend();
		}
	}
	// TODO:
	//		Implement better, safer, iterators.
	// -
	++size;
	first.set_max(size);
	last.set_max(size);
	++last;
	data[last] = item;
}

template <typename T>
T Ring<T>::pop ()
{
	// Return value.
	// -
	T item = data[first];
	++first;
	--size;
	return item;
}

template <typename T>
T Ring<T>::operator [] (const uint8_t& index) const
{
	// Iterator does the addition.
	// -
	return data[first + index];
}

template <typename T>
void Ring<T>::extend ()
{
	// Allocate space.
	// -
	T* new_data = 0;
	uint8_t new_capacity = 2 * size;
	new_data = static_cast<T*>(malloc(new_capacity * sizeof(T)));
	// Could not allocate memory.
	// -
	if (new_data == 0)
	{
		// Blink error.
		// -
	}
	// Copy data to new location.
	// -
	{
		uint8_t i = 0;
		do {
			new_data[i] = data[first];
			++first;
		} while (first != last);
	}
	// Update variables..
	// Same size. New capacity.
	// -
	first = 0;
	last = size - 1;
	capacity = new_capacity;
	// .. and free previous data.
	// -
	T* old_data = data;
	data = new_data;
	free(old_data);
}

// Explicit instantiation.
// -
extern template class Ring<uint16_t>;