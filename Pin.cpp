#include "Pin.h"

Pin::Pin (volatile uint8_t& DDRX, volatile uint8_t& PORTX, const uint8_t& pin_number)
	: ddr(DDRX), port(PORTX), mask(1 << pin_number)
{
	// Data Direction Register.
	// -
	ddr |= mask;
}

void Pin::toggle ()
{
	port ^= mask;
}

Pin& Pin::operator = (const bool& state)
{
	if (state == true)
	{
		port |= mask;
	}
	else
	{
		port &= ~mask;
	}
	return *this;
}
