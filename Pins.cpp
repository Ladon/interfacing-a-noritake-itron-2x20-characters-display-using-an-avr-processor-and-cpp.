#include "Pins.h"

Pins::Pins (volatile uint8_t& DDRX, volatile uint8_t& PORTX, const char bit_order[])
	: ddr(DDRX), port(PORTX), mask(0), map(bit_order)
{
	// Trick.
	// -
	mask = map(0xFF);
	// Set as output.
	// -
	ddr |= mask;
}

Pins& Pins::operator = (const uint8_t& data)
{
	port &= ~mask;
	port |= map(data);
	return *this;
}
