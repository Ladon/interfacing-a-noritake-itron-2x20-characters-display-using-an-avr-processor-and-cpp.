#pragma once
#include "Display.h"
#include "Ring.h"

/*
 * Description:
 * 		Uses display as ADC indicator shifting the former leftwards.
 * -
 * */
class Oscilloscope
{
	using T = uint16_t;
public:
	explicit Oscilloscope(const Display& tmp_display);
	~Oscilloscope () 									= default;
	Oscilloscope () 									= delete;
	Oscilloscope (const Oscilloscope&) 					= delete;
	Oscilloscope (Oscilloscope&&) 						= delete;
	Oscilloscope& operator = (const Oscilloscope&) 		= delete;
	Oscilloscope& operator = (Oscilloscope&&) 			= delete;
	void run ();
	void pause ();
	void push (const T& data)
	{
//		display.set_cursor(0,19);
		display.print_char(value_to_glyph(data,0));
//		display.set_cursor(1,19);
//		display.print_char(value_to_glyph(data,1));
	}
	void tmp_write (const T& tmp_data);
	void tmp_update ();
private:
	Display display;
	// Arduino (Leonardo (atmega32u4) has 10-bit ADC.
	// Display is 20 characters wide.
	// -
	Ring<T> data;
	char buffer[20];
	uint8_t value_to_glyph(const T& value, const uint8_t& line = 0) const;
};
