#include "Array.h"
#include <ctype.h>		// isdigit()..
#include <stdlib.h>		// strtoul()..
//#include "Stack.h"

template <>
Array<uint8_t>::Array (const char numbers[])
{
	Stack stack = extract_numbers(numbers);
	array = static_cast<uint8_t*>(stack);
	size = stack.length();
}

template <>
Array<uint8_t>::Array (const uint8_t& length)
{
	array = static_cast<uint8_t*>(malloc(length * sizeof(uint8_t)));
	size = length;
}

template <>
Array<uint8_t>::~Array ()
{
	free(array);
}

template <>
uint8_t Array<uint8_t>::operator [] (const uint8_t& index) const
{
	return array[index];
}

/*template <>
uint8_t Array<uint8_t>::length () const
{
	return size;
}*/

template <typename T>
Stack Array<T>::extract_numbers (const char numbers[])
{
	Stack stack;
	const char* tmp = numbers;
	char* end;
	while (*tmp)
	{
		if (isdigit(*tmp))
		{
			stack.push(strtoul(tmp, &end, 10));
			tmp = end;
		}
		else
		{
			++tmp;
		}
	}
	return stack;
}
