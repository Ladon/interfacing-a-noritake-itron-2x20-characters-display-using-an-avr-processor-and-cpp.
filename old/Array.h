#pragma once
#include <stdint.h>		// uint8_t..
#include "Stack.h"

/*
 * Description:
 * 		Extracts and stores numbers from a string.
 * -
 * */
template <typename T = uint8_t>
class Array
{
public:
	Array (const char numbers[]);
	Array (const uint8_t& length);
	~Array ();
	T operator [] (const uint8_t& index) const;
//	uint8_t length () const;
	operator uint8_t () const
	{
		return size;
	}
private:
	T* array;
	uint8_t size;
	Stack extract_numbers (const char numbers[]);
};
