#pragma once
#include <stdint.h>		// uint8_t..

/*
 * To do:
 * 		Use a desctructible array as storage.
 * */
class Stack
{
public:
	Stack ();
	void push (const uint8_t& item);
	// Re-allocate memory.
	// -
	void increase ();
	// Return an array copy.
	// -
	explicit operator uint8_t* () const;
	uint8_t length() const;
private:
	uint8_t* array;
	uint8_t size;
	uint8_t stored;
};
