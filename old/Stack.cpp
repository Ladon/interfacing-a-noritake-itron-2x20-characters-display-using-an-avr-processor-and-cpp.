#include "Stack.h"
#include <stdlib.h>		// malloc()..

Stack::Stack () : array(nullptr), size(8), stored(0)
{
	array = static_cast<uint8_t*>(malloc(size*sizeof(uint8_t)));
	if (array == nullptr)
	{
		// Blink error.
		// -
	}
}

void Stack::push (const uint8_t& item)
{
	if (stored + 1 > size)
	{
		increase();
	}
	array[stored] = item;
	++stored;
}

void Stack::increase ()
{
	uint8_t* new_array = static_cast<uint8_t*>(realloc(array, 2 * size));
	if (new_array == NULL)
	{
		// Blink error.
		// -
	}
	size *= 2;
}

Stack::operator uint8_t* () const
{
	uint8_t* new_array = static_cast<uint8_t*>(malloc(sizeof(array)));
	for (uint8_t i = 0; i < stored; ++i)
	{
		new_array[i] = array[i];
	}
	return new_array;
}

uint8_t Stack::length() const
{
	return stored;
}
