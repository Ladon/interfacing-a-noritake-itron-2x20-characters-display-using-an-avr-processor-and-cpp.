#include "my_string.h"
#include <ctype.h>		// isdigit().. //
#include <stdlib.h>		// atoi().. //

void skip_space (const char* (&str))
{
	while (*str and isspace(*str))
	{
		++str;
	}
}

void skip_digit (const char* (&str))
{
	skip_space(str);
	while (*str and isdigit(*str))
	{
		++str;
	}
}

void skip_graph (const char* (&str))
{
	skip_space(str);
	while (*str and isgraph(*str))
	{
		++str;
	}
}

int8_t parse_int (const char* (&str))
{
	skip_space(str);
	if (!isdigit(*str))
	{
		return -1;
	}
	return atoi(str);
}
