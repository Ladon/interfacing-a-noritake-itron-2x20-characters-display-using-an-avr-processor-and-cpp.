#pragma once
#include "Pin.h"

class LED
{
public:
	explicit LED (const Pin& ppin);
	~LED () 						= default;
	LED () 							= delete;
	LED (const LED&) 				= delete;
	LED (LED&&) 					= delete;
	LED& operator = (const LED&) 	= delete;
	LED& operator = (LED&&) 		= delete;
	void toggle ();
	// "Print" 1 byte using blinks:)
	// -
	LED& operator () (uint8_t error_code, const uint16_t& period = 325);
	void blink (const uint16_t& ms = 50);
private:
	Pin pin;
};

// To do: Ensure single copy per pin.
// -
extern LED error;
