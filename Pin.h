#pragma once
#include <stdint.h>	// uint8_t.. //

/*
 * Description:
 * 		Adds intuitive functions to pin handling.
 * -
 * */
class Pin
{
public:
	Pin (volatile uint8_t& DDRX, volatile uint8_t& PORTX, const uint8_t& pin_number);
	Pin (const Pin&) 				= default;
	~Pin () 						= default;
	Pin () 							= delete;
	Pin (Pin&&) 					= delete;
	Pin& operator = (const Pin&)	= delete;
	Pin& operator = (Pin&&)			= delete;
	void toggle ();
	Pin& operator = (const bool& state);
private:
	volatile uint8_t& ddr;
	volatile uint8_t& port;
	const uint8_t mask;
};
