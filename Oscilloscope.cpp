#include "Oscilloscope.h"
#include "Gifs.h"
#include <stdlib.h>			// strlen().. //

Oscilloscope::Oscilloscope(const Display& tmp_display)
	: display(tmp_display), data(20)
{
	display.print("Good Day!");
//	display.set
	display.store_gif(Bars);
	display.set_cursor_display_shift(true,true);
//	display.set_cursor(0,19);
}

void Oscilloscope::tmp_write (const T& tmp_data)
{
	data.push(tmp_data);
}

void Oscilloscope::tmp_update ()
{
	// Print first line.
	// -
	for (uint8_t i = 0; i < 20; ++i)
	{
		buffer[i] = value_to_glyph(data[i]);
	}
	display.print(buffer,20,0,0);

	// Print second line.
	// -
	for (uint8_t i = 0; i < 20; ++i)
	{
		buffer[i] = value_to_glyph(data[i],1);
	}
	display.print(buffer,20,0,1);
}

/*
 * ADC is 10-bit -> 0..1023.
 * Glyphs are 8.
 * Lines are 2.
 * 		Lines are reversed. 0 -> top, 1 -> bottom.
 * -
 * */
uint8_t Oscilloscope::value_to_glyph(const T& value, const uint8_t& line) const
{
	/*
		States:
					  V < 512   |   512 <= V
					,-----------|------------.
			Line 0 |      0     | scale(0->7) |
			Line 1 | scale(0->7)|      7      |
				    '-----------"-------------'
	 */
	uint16_t ret;
	const uint8_t space = 0x20;
	if (value < 512 and line == 0)
	{
		ret = space;
	}
	else if (512 <= value and line != 0)
	{
		ret = 7;
	}
	else
	{
		ret = value;
		if (line == 0)
		{
			ret -= 512;
		}
		ret = (ret * 7) / 511;
	}
/*
	// Return space (empty char).
	// -
	if (value == 0)
	{
		ret = empty;
	}
	// Empty char.
	// -
	else if (line == 0 and value < 512)
	{
		ret = empty;
	}
	// Full char.
	// -
	else if (line != 0 and 512 <= value)
	{
		ret = 7;
	}
	else
	{
		ret = value;
		// Get the upper half of 1023 for line 0.
		// -
		ret -= line ? 0 : 512;
		// Scale from 1 (0) to 8 (7).
		// -
		// Choose 1 of 8 glyphs.
		// -
		// TODO:
		// 		Choose whether to avoid floating point calculations.
		// -	
		ret = (ret * 7) / 511;
	}
	*/
	return ret;
}