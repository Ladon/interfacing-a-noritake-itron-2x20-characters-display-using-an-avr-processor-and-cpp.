#pragma once

#include <stdint.h>		// uint8_t.. //

extern const uint8_t Rocket[8][8];
extern const uint8_t Rocket_Pure[8][8];
extern const uint8_t Dance[8][8];
extern const uint8_t Circle[8][8];
extern const uint8_t Bars[8][8];
