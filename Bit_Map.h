#pragma once
#include "stdint.h"

/*
 * Description:
 * 		Translates an input byte according to a mapping that it was initialized with.
 * -
 * */
class Bit_Map
{
public:
	// Pre-condition:
	// 		Argument has exactly 8 elements some of which may be numbers.
	// -
	explicit Bit_Map (const char tmp_bit_order[]);
	Bit_Map (const Bit_Map&) 				= default;
	~Bit_Map () 							= default;
	Bit_Map () 								= delete;
	Bit_Map (Bit_Map&&) 					= delete;
	Bit_Map& operator = (const Bit_Map&) 	= delete;
	Bit_Map& operator = (Bit_Map&&) 		= delete;
	// Translate data to proper bit order.
	// -
	uint8_t operator () (const uint8_t& tmp_data) const;
private:
	uint8_t bit_order[8];
	void set_bit_order (const char bit_order_copy[]);
};
